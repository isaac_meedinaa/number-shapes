package example.isaac.numbershapes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public class Number {
        int number;

        public boolean isSquare() {
            double squareRoot = Math.sqrt(number);

            if (squareRoot == Math.floor(squareRoot)) {
                return true;
            } else {
                return false;
            }
        }

        public boolean isTriangular() {
            int x = 1;
            int triangularNumber = 1;

            while (triangularNumber < number) {
                x++;
                triangularNumber = triangularNumber + x;
            }

            if (triangularNumber == number) {
                return true;
            } else {
                return false;
            }
        }
    }

    public void checkNumberClick(View view) {
        EditText numberEditText = (EditText) findViewById(R.id.et_one);
        String message;

        if (numberEditText.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please input a number!", Toast.LENGTH_SHORT).show();
        } else {
            Number myNumber = new Number();
            myNumber.number = Integer.parseInt(numberEditText.getText().toString());
            message = numberEditText.getText().toString();

            if (myNumber.isSquare() && myNumber.isTriangular()) {
                Toast.makeText(this, message + " is both square and triangular.", Toast.LENGTH_LONG).show();
            } else if (myNumber.isSquare()) {
                Toast.makeText(this, message + " is square.", Toast.LENGTH_LONG).show();
            } else if (myNumber.isTriangular()) {
                Toast.makeText(this, message + " is triangular.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Input a valid number.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
